# Setup

Required tooling: 
 * [node](https://nodejs.org/en/download/)
 * [git](https://git-scm.com/downloads) 

Any text editor works, but if you dont have none try [Visual Studio Code](https://code.visualstudio.com/) 

 * jest getting started (unit testing) - https://jestjs.io/docs/en/getting-started
 * eslint (linter we used) - https://eslint.org/

# Run project

```sh
# install node packages
npm install

# run linter
npm run lint

# run unit testing (jest)
npm run test
```
