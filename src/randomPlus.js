const random = require("./random");

function randomPlus(num) {
    return random(20) + num;
}

module.exports = randomPlus;
