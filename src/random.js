function random(maxNumber = 10) {
    return Math.floor(Math.random() * maxNumber);
}

module.exports = random;