it('matchers', function() {
    expect(1).toBeTruthy();
    expect('').toBeFalsy();
    expect(1).not.toBeFalsy();
    const obj = {one: 1, two: 2}
    expect(obj).toEqual({two: 2, one: 1})
});